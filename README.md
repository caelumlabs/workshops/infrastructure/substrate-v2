# substrate-v2

Workshops and documentation related to LivingLabs substrate network

In this repository you can find the following information:

LivingLabs Testnet:
- How to participate to the network creating a node
- Procedure to request tokens (in order to use extrinsics/nomination or validation actions)
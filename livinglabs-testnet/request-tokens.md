- [Description](#description)
  - [Creating an account (I didn't have one)](#creating-an-account-i-didnt-have-one)
  - [Requesting tokens](#requesting-tokens)

# Description

Procedure to request tokens for an account 
  - Use extrinsics 
  - Nominate other nodes
  - Apply for Node Validator role

## Creating an account (I don't have one)

We can create it following these steps:

  - Open [PolkadotJS]: https://polkadot.js.org/apps/#/settings

  - Enable Custom endpoint and add our substrate workshop network as "Remote Node/Endpoint"
  
  If you don't have a node with a specific endpoint, use this one (CaelumLabs):
  ```
  wss://livinglabs-testnet.caelumlabs.com
  ```
    
  - Create Personal Account (**Save your Password and store the backup file provided**)
  - ![WALLET](images/workshop-account-stash.png)

## Requesting tokens

  - Request your tokens in the official Slack platform channel typing your account public address
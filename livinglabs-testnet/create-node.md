- [Description](#description)
- [Installation](#installation)
  - [Install docker](#install-docker)
- [Run your node](#run-your-node)
- [Monitoring Tools](#monitoring-tools)

# Description

Procedure to deploy substrate node in LivingLabs Testnet

# Installation

## Install docker

[Documentation]: https://docs.docker.com/install
[Post Installation]: https://docs.docker.com/install/linux/linux-postinstall/

```
sudo apt-get update
sudo apt install docker.io
sudo systemctl start docker
docker --version
```

If you don’t want to preface the docker command with sudo, create a Unix group called docker and add users to it. When the Docker daemon starts, it creates a Unix socket accessible by members of the docker group.

```
sudo groupadd docker
sudo usermod -aG docker

# logout & login
```

* Download CaelumLabs Substrate Docker image

```
docker pull eu.gcr.io/caelumlabs-hub01/substrate-apps/substrate-v2-lorena:0ffd085fb2ea8feb062f337ec9c50cf8be47d9cc
```

# Run your node

In order to create a node,  **--name ${MyName}** parameter value must be changed.

```
docker run --rm -it -p 30333:30333/tcp -p 30333:30333/udp -p 9944:9944/tcp \
  --name **${MyName}** eu.gcr.io/caelumlabs-hub01/substrate-apps/substrate-v2-lorena:0ffd085fb2ea8feb062f337ec9c50cf8be47d9cc \
  ./target/release/substrate \
  --chain ./specfiles/livinglabs-testnet-specfileRaw.json  \
  --base-path /data \
  --name **${MyName}** \
  --bootnodes /ip4/104.199.49.85/tcp/30333/p2p/QmPrWStPRQXcSmEGASm2hoXnfHGgokezowu6zoB9NL3zLc
```

If websocket port (9944) must be visible to external network, the flags **--unsafe-ws-external** and **--rpc-cors all** must be added:

```
docker run --rm -it -p 30333:30333/tcp -p 30333:30333/udp -p 9944:9944/tcp \
  --name **${MyName}** eu.gcr.io/caelumlabs-hub01/substrate-apps/substrate-v2-lorena:0ffd085fb2ea8feb062f337ec9c50cf8be47d9cc \
  ./target/release/substrate \
  --chain ./specfiles/livinglabs-testnet-specfileRaw.json  \
  --base-path /data \
  --name **${MyName}** \
  --bootnodes /ip4/104.199.49.85/tcp/30333/p2p/QmPrWStPRQXcSmEGASm2hoXnfHGgokezowu6zoB9NL3zLc \
  --unsafe-ws-external \
  --rpc-cors all
```

# Persistent Storage

To provide persistent storage probably you will need add the option [volume or mount](https://docs.docker.com/storage/volumes/) on docker run command. Example mount data docker volume on docker "/data" directory (/data is our substrate base path):

```
--mount source=data,target=/data
```

# Monitoring Tools

- Polkadot JS - Blockchain Explorer : https://polkadot.js.org/apps/#/settings
- Telemetry - Nodes Explorer: https://telemetry.polkadot.io/#list/LivingLabs%20Testnet
